# **Ansible Project**

Ce projet utilise Ansible pour automatiser le déploiement d'une installation WordPress sur un serveur Linux. Il configure l'environnement nécessaire pour faire fonctionner WordPress, y compris PHP, MySQL/MariaDB, et Apache/Nginx, installe WordPress et le configure avec des clés uniques et des salts pour renforcer sa sécurité.

## Prérequis

- Deux serveurs Linux (Debian/Ubuntu recommandé) avec accès SSH.
- Ansible installé sur votre machine de contrôle.
- Accès root ou un utilisateur avec des privilèges sudo sur les serveur cible.
- Git installé sur votre machine de contrôle pour cloner ce dépôt.

## Installation et Configuration

### 1. Cloner le Dépôt

   Pour commencer, clonez ce dépôt sur votre machine de contrôle avec Git :

   ```bash
   git clone https://gitlab.com/Mylark/ansible.git
   cd ansible
   ```


### 2. Configurer l'Inventaire Ansible

   Modifiez le fichier hosts pour ajouter l'adresse IP de vos serveurs cibles sous les groupes [wordpress_servers] ainsi que [database_servers].

   [wordpress_servers]

   192.168.187.136 ansible_user=votre_utilisateur

   [database_servers]
   
   192.168.187.137 ansible_user=votre_utilisateur



### 3. Exécuter le Playbook

   Lancez le playbook Ansible avec la commande suivante :

   ```bash
   ansible-playbook -i hosts playbook.yml
   ```

## Caractéristiques

- Installation Automatique de WordPress : Configure automatiquement un serveur avec les dépendances nécessaires et déploie la dernière version de WordPress.
- Configuration Sécurisée : Génère des clés uniques et des salts pour WordPress et configure wp-config.php en conséquence.
- Gestion des Permissions : Configure les permissions des fichiers et dossiers de WordPress pour renforcer la sécurité et la fonctionnalité.
- Installation Automatique de MySQL : Configure un serveur MySQL/MariaDB et crée une base de données pour WordPress.


## Personnalisation

Vous pouvez personnaliser ce projet en modifiant les variables dans le dossier vars des rôles Ansible pour correspondre à vos besoins spécifiques, tels que le nom de la base de données WordPress, l'utilisateur, et le mot de passe.

## Contribution

Les contributions à ce projet sont les bienvenues. Veuillez créer une issue ou une pull request pour toute fonctionnalité supplémentaire ou correction de bug.

## Licence

Ce projet est sous licence MIT. Voir le fichier LICENSE pour plus de détails.